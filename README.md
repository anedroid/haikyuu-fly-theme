Haikyuu-Fly theme based on original strange_wilds's [Haikyuu-Karasuno Fly Theme](https://addons.mozilla.org/addon/haikyuu-fly-theme) and Wimon's [Karasuno Theme-Haikyuu](https://addons.mozilla.org/pl/firefox/addon/karasuno-theme-haikyuu). The image is from the first one, but color theming is a mix of those two. If you like it, or maybe suggest something to change, give me a feedback in this repo's issues or by the fediverse: <https://mstdn.social/@anedroid>.

I think this theme would look better with a half-transparent white toolbar, but as of IceCat 91.9.0 ESR if `theme.colors.toolbar` is set to any light color, it displays everything in light theme, even if `theme.properties.color_scheme` and `theme.properties.content_color_scheme` are set to `dark`. This is a bug in my opinion.

![theme screenshot](screenshot.png)
